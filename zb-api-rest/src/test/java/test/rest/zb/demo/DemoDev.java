package test.rest.zb.demo;

import java.io.File;
import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import entity.commons.UserApi;
import entity.enumtype.ExchangeEnum;
import jodd.props.Props;
import rest.zb.rest.api.RestApiGet;
import rest.zb.rest.api.RestApiPost;
import rest.zb.rest.entity.post.Account;

/**
 * 测试环境
 * @author Administrator
 *
 */
public class DemoDev {
	private final String url_get = "http://ttapi.zb.com/data/v1";
//	private final String url_post = "http://tttrade.zb.com/api/";
	private final String url_post = "http://127.0.0.1:9481/api/";
	
	private RestApiPost apiPost;
	private RestApiGet apiGet;
	
	
//	@Test
	public void 交易api() {	
		// 遍历用户信息
		Account account = apiPost.getAccount();
		account.getResult().getCoins().forEach(coin -> {
			System.out.println("可用:" + coin.getAvailable() + ",冻结:" + coin.getFreez());
		});
	}
	
	@Test
	public void 子账户() {
//		apiPost.addSubUser();
		
		//{"code":1000,"message":"操作成功","result":[{"isOpenApi":false,"memo":"dfdfd","pwdLevel":0,"userName":"13415000099@test12","userId":358675,"isFreez":false}]}
//		apiPost.getSubUserList();
		
		apiPost.createSubUserKey();
		
	}
	
	
	@Before
	public void init() throws IOException {
		String symbol = "ltc_usdt";
		// 构造行情api对象
		apiGet = new RestApiGet(url_get, symbol);
		Props p = new Props();
		p.load(new File("c:/config/zb_testev_13415000099.txt"));
		String apiKey = p.getValue("user.zb.apikey");// 修改为自己的公钥
		String secretKey = p.getValue("user.zb.secretKey");// 修改为自己的私钥
		
		// 构造交易接口对象
		apiPost = new RestApiPost(url_post, symbol, new UserApi(ExchangeEnum.zb, "测试", apiKey, secretKey));

	}
}
